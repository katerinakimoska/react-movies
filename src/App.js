import React, { Component } from "react";
import Movie from "./Components/Movie";
import "./App.css";

export default class App extends Component {
  state = {
    query: "",
    movie: [],
    listed: false,
  };

  updateQuery(e) {
    this.setState({
      query: e.target.value.toLowerCase(),
    });
  }

  findApi(e) {
    this.state.query && fetch(`https://imdb-api.com/en/API/SearchSeries/k_l4pi24lo/${this.state.query}`)
        .then((rez) => rez.json())
        .then((data) => {
          this.setState({
            movie: data.results[0],
            listed:true,
            query: "",
          });
          console.log(this.state.movie);
        });

  }

  render() {
    return (
      <div className="App">
        <div className="search-part">
        <h1>Search your favourite series :</h1>
        <input
        className="input-search"
          type="text"
          value={this.state.query}
          onChange={(e) => this.updateQuery(e)}
        ></input>
        <br/>
        <button className="btn-search" onClick={(e) => this.findApi(e)}>Search</button>
        </div>
        <div className="movie-part">
          {this.state.listed && <Movie list={this.state.movie} ></Movie>}
        </div>
      </div>
    );
  }
}
