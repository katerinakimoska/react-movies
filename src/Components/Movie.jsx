import React, { Component } from "react";


export default class Movie extends Component {
  state = {
    id: "",
    moviedetails: [],
    opened: false,
  };

  componentDidMount() {
    this.setState({
      id: this.props.list.id,
    });
  }

  showDetails(e) {
    if (this.state.opened === false) {
      this.setState({
        opened: true,
      });
    } else {
      this.setState({
        opened: false,
      });
    }

    this.state.id &&
      fetch(
        `${this.props.link}/Title/${this.props.code}/${this.state.id}/FullActor,Ratings`
        
      )
        .then((rez) => rez.json())
        .then((data) => {
          this.setState({
            moviedetails: data,
          });
        });
  }

  render() {
    const { id, image, title, description } = this.props.list;
    const { stars, ratings } = this.state.moviedetails;
    console.log(ratings);
    // console.log( typeof ratings);


    return (
      <div className="cards">
        <img src={image} alt="series" className="image"></img>
        <div className="text">
          <p>
            <span>ID:</span> {id}{" "}
          </p>
          <p>
            {" "}
            <span>Title:</span> {title}{" "}
          </p>
          <p>
            <span>Year:</span> {description}{" "}
          </p>
          <button className="details" onClick={(e) => this.showDetails(e)}>
            Click for more details
          </button>
          {this.state.opened && (
            <div className="details-part">
              <h3>{title}</h3>
              <img src={image} alt="series" className="image" />
              <p>
                {" "}
                <span>Actors list:</span> {stars}
              </p>
              <div>
                {ratings && (
                  <div>
                    <p>
                      <span>Ratings:</span>
                    </p>
                    <p>{ratings.imDb && `imDb: ${ratings.imDb} / 10`}</p>
                    <p>
                      {ratings.rottenTomatoes &&
                        `rottenTomatoes: ${ratings.rottenTomatoes}% / 100%`}
                    </p>
                    <p>
                      {ratings.metacritic &&
                        `metacritic: ${ratings.metacritic} / 100`}
                    </p>
                    <p>{ratings.tV_com && `tV_com: ${ratings.tV_com} / 10`}</p>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
